export const articles = [
  {
    "id": 1,
    "title": "Cars",
    "description": "Cars is a 2006 American computer-animated comedy film produced by Pixar Animation Studios and released by Walt Disney Pictures. The film was directed by John Lasseter from a screenplay by Dan Fogelman, Lasseter, Joe Ranft, Kiel Murray, Phil Lorin, and Jorgen Klubien and a story by Lasseter, Ranft, and Klubien, and was the final film independently produced by Pixar after its purchase by Disney in January 2006. Set in a world populated entirely by anthropomorphic talking cars and other vehicles, the film stars the voices of Owen Wilson, Paul Newman (in his final acting role), Bonnie Hunt, Larry the Cable Guy, Tony Shalhoub, Cheech Marin, Michael Wallis, George Carlin, Paul Dooley, Jenifer Lewis, Guido Quaroni, Michael Keaton, Katherine Helmond, John Ratzenberger and Richard Petty, while race car drivers Dale Earnhardt Jr. (as\n    Junior), Mario Andretti, Michael Schumacher and car enthusiast Jay Leno (as Jay Limo) voice themselves.",
    "image": "https://www.ford.com/cmslibs/content/dam/brand_ford/en_us/brand/legacy/marketing/brand-gallery/21_FRD_EPR_53020_KingRanch_1080x960.jpg/_jcr_content/renditions/cq5dam.web.1280.1280.jpeg"
  },
  {
    "id": 2,
    "title": "Bikes",
    "description": "Huge Moto is a custom motorcycle design and fabrication shop run by a handful of motorcycle addicts with industrial design and engineering backgrounds.  A spin-off from a successful product design firm located in San Francisco, CA.. HUGE DESIGN.It all started in Bill Webb's garage back in 2014 when he recovered his stolen and stripped 1995 Kawasaki Ninja ZX7. With the fairings and safety equipment gone, he saw the potential to build a beautiful street machine: the CAFE FIGHTER (see below).  Based on the success and positive reaction to the Ninja, the team doubled-down and created two more cafe fighter builds based on a more modern Honda CBR 1000rr race bike.  The focus on the CBR builds was to maintain the factory performance while creating a more urban, retro-future street look.",
    "image": "https://images.squarespace-cdn.com/content/v1/57bb6bb33e00be03ee971ee2/1473956834830-INVLZ7TUDNDGVBU97D68/ke17ZwdGBToddI8pDm48kH32ktDnH8H04O3hrKeqOkd7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UahApXywHVQ_ir7Fd3700G7NokeHPX3y5HUu7Y_Ix2u-CPaHLWQWi2MOE7MWfY5Q9A/ninja_02.jpg?format=1000w"
  },
  {
    "id": 3,
    "title": "Planes",
    "description": "The Lockheed Martin F-35 Lightning II is an American family of single-seat, single-engine, all-weather stealth multirole combat aircraft that is intended to perform both air superiority and strike missions. It is also able to provide electronic warfare and intelligence, surveillance, and reconnaissance capabilities. Lockheed Martin is the prime F-35 contractor, with principal partners Northrop Grumman and BAE Systems. The aircraft has three main variants: the conventional takeoff and landing F-35A (CTOL), the short take-off and vertical-landing F-35B (STOVL), and the carrier-based F-35C (CV/CATOBAR).",
    "image": "https://pbs.twimg.com/media/DxDEmd1XQAENKUA.jpg:large"
  }
];
