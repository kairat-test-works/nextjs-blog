import {articles} from 'fixtures/articles';


export default function handler(req, res) {
  const {
    query: {id},
  } = req

  try {
    const articleId = parseInt(id, 10)

    const article = articles.find(_article => _article.id === articleId)

    if (article) {
      res.status(200).json(article);
    } else {
      res.status(404).end(`Article with id=${id} not found`);
    }
  } catch (e) {
    res.status(500).end(`Server error`);
  }
}
