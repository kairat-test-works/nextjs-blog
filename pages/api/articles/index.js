import {articles} from 'fixtures/articles';


export default function handler(req, res) {
  res.status(200).json(articles);
}

