export async function getStaticProps() {
  return {
    redirect: {
      destination: '/articles',
      permanent: true,
    },
  }
}

