import Link from 'next/link'
import Image from 'next/image'


function Article(props) {
  const {
    data
  } = props

  return (
    <>
      <h1>{data.title}</h1>
      <p>{data.description}</p>
      <div>
        <Image src={data.image} width={300} height={200}/>
      </div>
      <Link href="/articles">Go back</Link>
    </>
  )
}

export async function getStaticProps(context) {
  const {
    params
  } = context

  let data = {}

  const resp = await fetch(`${process.env.API_URL}/articles/${params.id}`)

  if (resp.ok) {
    data = await resp.json()
  } else {
    return {
      notFound: true
    }
  }

  return {
    props: {
      data
    },
  }
}

export async function getStaticPaths() {
  const resp = await fetch(`${process.env.API_URL}/articles`)
  const data = await resp.json()

  const paths = data.map((article) => ({
    params: {
      id: `${article.id}`
    },
  }))

  return {
    paths,
    fallback: false
  }
}

export default Article
