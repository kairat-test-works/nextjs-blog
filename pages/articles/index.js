import Link from 'next/link'


function Articles(props) {
  const {
    data
  } = props

  return (
    <ul>
      {data.map((article, idx) => (
        <li key={`article-${idx}`}>
          <Link href="/articles/[id]" as={`/articles/${article.id}`}>
            <a>{article.title}</a>
          </Link>
        </li>
      ))}
    </ul>
  );
}

export async function getStaticProps() {
  const resp = await fetch(`${process.env.API_URL}/articles`)
  const data = await resp.json()

  return {
    props: {
      data
    },
  }
}

export default Articles
